<?php

namespace MyLocalBundle\Controller;

use MyLocalBundle\Entity\Messages;
use MyLocalBundle\Entity\Users;
use MyLocalBundle\Util\Push;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Tools\Pagination\Paginator;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class SuperChatController extends Controller
{

    public function createUserAction(Request $request)
    {
        $utils = $this->get('main.util.mygcmutils');
        $utils->LogMethodIn($request, 'createUserAction');
        $em = $this->getDoctrine()->getManager();
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $user = $serializer->deserialize($request->getContent(), 'MyLocalBundle\Entity\Users', 'json');
        $validator = $this->get('validator');
        $errors = $validator->validate($user);
        $code = 201;
        if (count($errors) > 0) {
            $answer = array();
            foreach ($errors as $error) {
                $answer[] = array($error->getPropertyPath() => $error->getMessage());
            }
            $code = 232;
            return new Response(
                $serializer->serialize($answer, 'json'),
                $code,
                array(
                    'content-type' => 'application/json',
                    'message' => 'Errorcitos'
                )
            );
        } else {
            $em->persist($user);
            $em->flush();
        }
        return new Response(
            $serializer->serialize($user, 'json'),
            $code,
            array(
                'content-type' => 'application/json'
            )
        );
    }

    public function updatePushAction(Request $request, $userId)
    {
        $utils = $this->get('main.util.mygcmutils');
        $utils->LogMethodIn($request, 'updatePushAction');

        $content = $request->getContent();
        $gcm_registration_id = json_decode($content, true)['gcm_registration_id'];

        $em = $this->getDoctrine()->getManager();
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $user = $em->getRepository('MyLocalBundle:Users')->find($userId);
        $user->setGcmRegistrationId($gcm_registration_id);
        $em->persist($user);
        $em->flush();
        return new Response(
            $serializer->serialize($user, 'json'),
            200,
            array(
                'content-type' => 'application/json'
            )
        );
    }

    public function getChatsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $chats = $em->getRepository('MyLocalBundle:ChatRooms')->findAll();
        return new Response(
            $serializer->serialize($chats, 'json'),
            200,
            array(
                'content-type' => 'application/json'
            )
        );
    }

    public function addMessageToChatAction(Request $request, $chatId)
    {
        $utils = $this->get('main.util.mygcmutils');
        $utils->LogMethodIn($request, 'addMessageToChatAction');

        $content = $request->getContent();
        $received = json_decode($content, true);

        $userId = $received['user_id'];
        $messageContent = $received['message'];

        $em = $this->getDoctrine()->getManager();

        $message = new Messages();
        $message->setChatRoom($em->getRepository('MyLocalBundle:ChatRooms')->find($chatId));
        $message->setMessage($messageContent);
        $message->setUser($em->getRepository('MyLocalBundle:Users')->find($userId));
        $em->persist($message);
        $em->flush();

        $push = new Push();

        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $data = array();
        $data['user'] = json_decode($serializer->serialize($message->getUser(), 'json'), true);
        $data['message'] = $message->getMessage();
        $data['chat_room_id'] = $message->getChatRoom()->getChatRoomId();
        $data['createdAt'] = $message->getCreatedAt();

        $push->setTitle("Google Cloud Messaging");
        $push->setIsBackground(FALSE);
        $push->setFlag($this->getParameter('PUSH_FLAG_CHATROOM'));
        $push->setData($data);

        // echo json_encode($push->getPush());exit;

        // sending push message to a topic
        $utils->sendToTopic('topic_' . $message->getChatRoom()->getChatRoomId(), $push->getPush());

        $newMessage = $em->getRepository('MyLocalBundle:Messages')->find($message->getMessageId());

        return new Response(
            $serializer->serialize($newMessage, 'json'),
            200,
            array(
                'content-type' => 'application/json'
            )
        );
    }

    public function addMessageForUserAction(Request $request, $targetId)
    {
        $utils = $this->get('main.util.mygcmutils');
        $utils->LogMethodIn($request, 'addMessageToChatAction');

        $content = $request->getContent();
        $received = json_decode($content, true);

        $userId = $received['user_id'];
        $messageContent = $received['message'];

        $em = $this->getDoctrine()->getManager();

        $message = new Messages();
        $message->setTargetUser($em->getRepository('MyLocalBundle:Users')->find($targetId));
        $message->setMessage($messageContent);
        $message->setUser($em->getRepository('MyLocalBundle:Users')->find($userId));
        $em->persist($message);
        $em->flush();

        $push = new Push();


        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $data = array();
        $data['user'] = json_decode($serializer->serialize($message->getTargetUser(), 'json'), true);
        $data['message'] = $message->getMessage();
        $data['image'] = '';
        $data['createdAt'] = $message->getCreatedAt();

        $push->setTitle("Google Cloud Messaging");
        $push->setIsBackground(FALSE);
        $push->setFlag($this->getParameter('PUSH_FLAG_USER'));
        $push->setData($data);

        // echo json_encode($push->getPush());exit;

        // sending push message to a topic
        $utils->send($message->getTargetUser()->getGcmRegistrationId(), $push->getPush());

        $newMessage = $em->getRepository('MyLocalBundle:Messages')->find($message->getMessageId());

        return new Response(
            $serializer->serialize($newMessage, 'json'),
            200,
            array(
                'content-type' => 'application/json'
            )
        );
    }

    public function addMessageForMultipleUsersAction(Request $request)
    {
        $utils = $this->get('main.util.mygcmutils');
        $utils->LogMethodIn($request, 'addMessageForMultipleUsersAction');

        $content = $request->getContent();
        $received = json_decode($content, true);

        $userId = $received['user_id'];
        $to_user_ids = array_filter(explode(',', $received['to']));
        $messageContent = $received['message'];

        $em = $this->getDoctrine()->getManager();
        $targetUsers = $em->getRepository('MyLocalBundle:Users')->findByUserId($to_user_ids);

        $registration_ids = array();

        // preparing gcm registration ids array
        foreach ($targetUsers as $u) {
            array_push($registration_ids, $u->getGcmRegistrationId());
        }
        // creating tmp message, skipping database insertion
        $msg = array();
        $msg['message'] = $messageContent;
        $msg['message_id'] = '';
        $msg['chat_room_id'] = '';
        $msg['created_at'] = date('Y-m-d G:i:s');

        $data = array();
        $data['user'] = $em->getRepository('MyLocalBundle:Users')->find($userId);
        $data['message'] = $msg;
        $data['image'] = '';

        $push = new Push();
        $push->setTitle("Google Cloud Messaging");
        $push->setIsBackground(FALSE);
        $push->setFlag($this->getParameter('PUSH_FLAG_USER'));
        $push->setData($data);

        // sending push message to multiple users
        $utils->sendMultiple($registration_ids, $push->getPush());


        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        return new Response(
            $serializer->serialize($targetUsers, 'json'),
            200,
            array(
                'content-type' => 'application/json'
            )
        );
    }

    public function addMessageForAllUsersAction(Request $request)
    {
        $utils = $this->get('main.util.mygcmutils');
        $utils->LogMethodIn($request, 'addMessageForAllUsersAction');

        $content = $request->getContent();
        $received = json_decode($content, true);

        $userId = $received['user_id'];
        $messageContent = $received['message'];

        $em = $this->getDoctrine()->getManager();
        // creating tmp message, skipping database insertion
        $msg = array();
        $msg['message'] = $messageContent;
        $msg['message_id'] = '';
        $msg['chat_room_id'] = '';
        $msg['created_at'] = date('Y-m-d G:i:s');

        $data = array();
        $data['user'] = $em->getRepository('MyLocalBundle:Users')->find($userId);
        $data['message'] = $msg;
        $data['image'] = 'http://www.androidhive.info/wp-content/uploads/2016/01/Air-1.png';

        $push = new Push();
        $push->setTitle("Google Cloud Messaging");
        $push->setIsBackground(FALSE);
        $push->setFlag($this->getParameter('PUSH_FLAG_USER'));
        $push->setData($data);

        // sending message to topic `global`
        // On the device every user should subscribe to `global` topic
        $utils->sendToTopic('global', $push->getPush());


        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        return new Response(
            $serializer->serialize($data, 'json'),
            200,
            array(
                'content-type' => 'application/json'
            )
        );
    }


    public function getChatAction(Request $request, $chatId)
    {
        $utils = $this->get('main.util.mygcmutils');
        $utils->LogMethodIn($request, 'getChatAction');

        $em = $this->getDoctrine()->getManager();

        $stringQuery = 'SELECT  m
        FROM MyLocalBundle:Messages m
        LEFT JOIN m.chatRoom cr
        LEFT JOIN m.user u
        WHERE cr.chatRoomId = :chatId';

        $query = $em->createQuery($stringQuery)
            ->setParameter('chatId', $chatId);

        $answer = $query->getResult();
        $encoders = array(new JsonEncoder());
        $normalizers = array((new ObjectNormalizer())->setIgnoredAttributes(array('chatRoom', 'targetUser', 'gcmRegistrationId')));
//        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        return new Response(
            $serializer->serialize($answer, 'json'),
            200,
            array(
                'content-type' => 'application/json'
            )
        );
    }

}
