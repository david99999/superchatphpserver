<?php

namespace MyLocalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;

class DefaultController extends Controller
{
    public function indexAction()
    {

        // return new Response(
        //     json_encode(    
        //         array('lucky_number' => rand(0, 100))
        //     ),
        //     200,
        //     array('Content-Type' => 'application/json')
        // );
        return $this->render('MyLocalBundle:Default:index.html.twig');
    }

    public function adminAction(Request $request)
    {
        // return $this->render("GustappMainBundle:Web:admin.html.twig");
        return $this->redirect($this->generateUrl("my_web_restaurant_info"));
    }

    public function loginAction(Request $request)
    {
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $request->getSession()->get(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render("MyLocalBundle:Web:login.html.twig", array(
            "error" => $error,
            'last_username' => $request->getSession()->get(SecurityContext::LAST_USERNAME),
        ));
    }

    public function restaurantInfoAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        return $this->render("MyLocalBundle:Web:admin_console.html.twig", array(
            "users" => $em->getRepository('MyLocalBundle:Users')->findAll(),
            'chatRooms' => $em->getRepository('MyLocalBundle:ChatRooms')->findAll()
        ));
    }


    public function secondAction()
    {

        return new Response(
            json_encode(
                array('lucky_number' => rand(0, 100))
            ),
            200,
            array('Content-Type' => 'application/json')
        );
        // return $this->render('MyLocalBundle:Default:index.html.twig');
    }
}
