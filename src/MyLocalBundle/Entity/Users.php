<?php

namespace MyLocalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use MyLocalBundle\Validator\Constraint as ValidationsAssert;
use Symfony\Component\Validator\Constraints as Assert;
// DON'T forget this use statement!!!
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Users
 *
 * @ORM\Table(name="users", uniqueConstraints={@ORM\UniqueConstraint(name="email", columns={"email"})})
 * @ORM\Entity
 * @UniqueEntity(
 *     fields={"email"},
 *     errorPath="email",
 *     message="Otro usuario por favor"
 * )
 * @ValidationsAssert\Users
 */
class Users
{
    /**
     * @var string
     *
     * @Assert\NotBlank(message="Dónde está el puto nombre?")
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string     *
     * @Assert\NotBlank(message="Dónde está el puto email?")
     * @Assert\Email(message="ponga un hp email válido!")
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Dónde está el puto token para las putas push?")
     * @ORM\Column(name="gcm_registration_id", type="text", length=65535, nullable=false)
     */
    private $gcmRegistrationId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false, columnDefinition="timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP")
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $userId;


    /**
     * Set name
     *
     * @param string $name
     * @return Users
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Users
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set gcmRegistrationId
     *
     * @param string $gcmRegistrationId
     * @return Users
     */
    public function setGcmRegistrationId($gcmRegistrationId)
    {
        $this->gcmRegistrationId = $gcmRegistrationId;

        return $this;
    }

    /**
     * Get gcmRegistrationId
     *
     * @return string
     */
    public function getGcmRegistrationId()
    {
        return $this->gcmRegistrationId;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Users
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        if ($this->createdAt != null)
            return $this->createdAt->getTimestamp();
        return null;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }
}
