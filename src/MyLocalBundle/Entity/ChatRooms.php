<?php

namespace MyLocalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ChatRooms
 *
 * @ORM\Table(name="chat_rooms")
 * @ORM\Entity
 */
class ChatRooms
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false, nullable=false, columnDefinition="timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP")
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="chat_room_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $chatRoomId;



    /**
     * Set name
     *
     * @param string $name
     * @return ChatRooms
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ChatRooms
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        if ($this->createdAt != null)
            return $this->createdAt->getTimestamp();
        return null;
    }

    /**
     * Get chatRoomId
     *
     * @return integer 
     */
    public function getChatRoomId()
    {
        return $this->chatRoomId;
    }
}
