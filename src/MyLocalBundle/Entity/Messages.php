<?php

namespace MyLocalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Messages
 *
 * @ORM\Table(name="messages", indexes={@ORM\Index(name="chat_room_id", columns={"chat_room_id"}), @ORM\Index(name="user_id", columns={"user_id"}), @ORM\Index(name="chat_room_id_2", columns={"chat_room_id"})})
 * @ORM\Entity
 */
class Messages
{
    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", length=65535, nullable=false)
     */
    private $message;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false, columnDefinition="timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="message_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $messageId;

    /**
     * @var \MyLocalBundle\Entity\Users
     *
     * @ORM\ManyToOne(targetEntity="MyLocalBundle\Entity\Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * })
     */
    private $user;

    /**
     * @var \MyLocalBundle\Entity\ChatRooms
     *
     * @ORM\ManyToOne(targetEntity="MyLocalBundle\Entity\ChatRooms")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="chat_room_id", referencedColumnName="chat_room_id")
     * })
     */
    private $chatRoom;


    /**
     * @var \MyLocalBundle\Entity\Users
     *
     * @ORM\ManyToOne(targetEntity="MyLocalBundle\Entity\Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="target_user_id", referencedColumnName="user_id")
     * })
     */
    private $targetUser;


    /**
     * Set message
     *
     * @param string $message
     * @return Messages
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Messages
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        if ($this->createdAt != null)
        return $this->createdAt->getTimestamp();
        return (new \DateTime())->getTimestamp();
    }

    /**
     * Get messageId
     *
     * @return integer 
     */
    public function getMessageId()
    {
        return $this->messageId;
    }

    /**
     * Set user
     *
     * @param \MyLocalBundle\Entity\Users $user
     * @return Messages
     */
    public function setUser(\MyLocalBundle\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \MyLocalBundle\Entity\Users 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set targetUser
     *
     * @param \MyLocalBundle\Entity\Users $user
     * @return Messages
     */
    public function setTargetUser(\MyLocalBundle\Entity\Users $user = null)
    {
        $this->targetUser = $user;

        return $this;
    }

    /**
     * Get targetUser
     *
     * @return \MyLocalBundle\Entity\Users
     */
    public function getTargetUser()
    {
        return $this->targetUser;
    }

    /**
     * Set chatRoom
     *
     * @param \MyLocalBundle\Entity\ChatRooms $chatRoom
     * @return Messages
     */
    public function setChatRoom(\MyLocalBundle\Entity\ChatRooms $chatRoom = null)
    {
        $this->chatRoom = $chatRoom;

        return $this;
    }

    /**
     * Get chatRoom
     *
     * @return \MyLocalBundle\Entity\ChatRooms 
     */
    public function getChatRoom()
    {
        return $this->chatRoom;
    }
}
