<?php

namespace MyLocalBundle\Util;

use Monolog\Logger;
use Symfony\Component\HttpFoundation\Request;
use Monolog\Handler\StreamHandler;

class GCMUtils
{

    private $GOOGLE_API_KEY = "";

    public function __construct($GOOGLE_API_KEY)
    {
        $this->GOOGLE_API_KEY = $GOOGLE_API_KEY;
    }

    // sending push message to single user by gcm registration id
    public function send($to, $message)
    {
        $fields = array(
            'to' => $to,
            'data' => $message,
        );
        return $this->sendPushNotification($fields);
    }

    // Sending message to a topic by topic id
    public function sendToTopic($to, $message)
    {
        $fields = array(
            'to' => '/topics/' . $to,
            'data' => $message,
        );
        return $this->sendPushNotification($fields);
    }

    // sending push message to multiple users by gcm registration ids
    public function sendMultiple($registration_ids, $message)
    {
        $fields = array(
            'registration_ids' => $registration_ids,
            'data' => $message,
        );

        return $this->sendPushNotification($fields);
    }

    // function makes curl request to gcm servers
    private function sendPushNotification($fields)
    {

        $this->log("Enviando notificacion con google_key: " .$this->GOOGLE_API_KEY);

        // Set POST variables
        $url = 'https://gcm-http.googleapis.com/gcm/send';

        $headers = array(
            'Authorization: key=' . $this->GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        //curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,1);

        $this->log("Iniciando curl");
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        $this->log("Curl terminado");
        $this->log("Resultado del curl:".$result);

        // Close connection
        curl_close($ch);

        return $result;
    }

    private function Log($line)
    {
        // create a log channel
        $log = new Logger ('chat_log');
        $log->pushHandler(new StreamHandler ('../app/logs/chat_log-' . date('d-m-Y') . '.log', Logger::INFO));
        // add records to the log
        $log->addInfo($line);
    }

    public function LogMethodIn(Request $request, $method)
    {
        $this->Log("entra al metodo $method");
        $this->Log("request -> " . $request);
    }

    public function LogOther($line)
    {
        $this->Log($line);
    }

    public function generateSHA1Token($rawPassword)
    {
        $salt = "TokenTemp";
        return sha1($salt . $rawPassword);
    }

    public function distance($lat1, $lon1, $lat2, $lon2)
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        return ($miles * 1.609344);
    }

}